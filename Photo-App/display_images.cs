﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Photo.Properties;

namespace Photo
{
    public partial class display_images : Form
    {

        public display_images()
        {
            InitializeComponent();
        }

        private void display_images_Load(object sender, EventArgs e)
        {
            string[] imgList = Directory.GetFiles("c:\\img_prueba", "*");

            bool primero = true;
            int x = 12;
            int y = 72;
            int contadorPic = 8;
            int i = 0;

            foreach (var item in imgList)
            {


                PictureBox pic = new PictureBox();

                pic.Name = "picc";

                pic.SizeMode = PictureBoxSizeMode.StretchImage;

                if ((i / contadorPic) == 1)
                {
                    y += 128;
                    contadorPic += 8;
                    x = 12;

                }

                if (primero)
                {

                    pic.Location = new Point(x, y);
                    primero = false;

                }
                else
                {
                    pic.Location = new Point(x, y);
                }

                x += 115;

                //other properties

                pic.BorderStyle = BorderStyle.FixedSingle;
                pic.Width = 109;
                pic.Height = 122;
                pic.Image = Image.FromFile(item);
                pic.Tag = item;
                pic.SizeMode = PictureBoxSizeMode.StretchImage;

                pic.Click += new System.EventHandler(pic_Click);

                this.Controls.Add(pic); //agregas el picturebox a la coleccion de controles del form

                i++;
            }

        }

        private void pic_Click(object sender, EventArgs e)
        {
            PictureBox pb = (sender as PictureBox);


            if (pb.BorderStyle == BorderStyle.FixedSingle)
            {
                pb.BorderStyle = BorderStyle.Fixed3D;
            }
            else
            {
                pb.BorderStyle = BorderStyle.FixedSingle;
            }


        }


        private void button2_Click(object sender, EventArgs e)
        {
            for (int u = 0; u < 20; u++)
            {
                foreach (PictureBox thecontrol in this.Controls.OfType<PictureBox>().Where((controlname) => controlname.Name.Contains("picc")))
                {
                    this.Controls.Remove(thecontrol);
                }
            }

            string[] imgList = Directory.GetFiles("c:\\img_prueba", "*");

            bool primero = true;
            int x = 12;
            int y = 72;
            int contadorPic = 8;
            int i = 0;

            foreach (var item in imgList)
            {


                PictureBox pic = new PictureBox();

                pic.Name = "picc";

                pic.SizeMode = PictureBoxSizeMode.StretchImage;

                if ((i / contadorPic) == 1)
                {
                    y += 128;
                    contadorPic += 8;
                    x = 12;

                }

                if (primero)
                {

                    pic.Location = new Point(x, y);
                    primero = false;

                }
                else
                {
                    pic.Location = new Point(x, y);
                }

                x += 115;

                //other properties

                pic.BorderStyle = BorderStyle.FixedSingle;
                pic.Width = 109;
                pic.Height = 122;
                pic.Image = Image.FromFile(item);
                pic.Tag = item;
                pic.SizeMode = PictureBoxSizeMode.StretchImage;

                pic.Click += new System.EventHandler(pic_Click);

                this.Controls.Add(pic); //agregas el picturebox a la coleccion de controles del form

                i++;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Photo
{
    public partial class Form1 : Form
    {
        public string photoPath = "";
        static string[] enlistado = new string[150];
        //static List<string> list = new List<string>(new string [99] );
        //List<PictureBox> itemsToRemove = new List<PictureBox>();
        PictureBox[] fotos = new PictureBox[90];
        int page;

        public Form1()
        {
            InitializeComponent();
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;            
        }      

        private void Button2_Click(object sender, EventArgs e)
        {
            //if (photoPath != "")
            //{
            PrintDocument recordDoc = new PrintDocument();
            recordDoc.BeginPrint += new System.Drawing.Printing.PrintEventHandler(recordDoc_BeginPrint);
            void recordDoc_BeginPrint(object o, System.Drawing.Printing.PrintEventArgs ev)
            {
                 page = 0;
            }
            PrinterSettings ps = new PrinterSettings();
            ps.DefaultPageSettings.Landscape = true;              
            recordDoc.PrinterSettings = ps;
            //recordDoc.DefaultPageSettings.PaperSize = new PaperSize("Polaroid 6 x 4", 600, 400);
            recordDoc.PrintPage += PrintPage;
            // pictureBox1.Image = (Image) Convert.printPreviewDialog1.Document;

            //printPreviewDialog1.Document = recordDoc;
            //printPreviewDialog1.PrintPreviewControl.Zoom = 1;
            //printPreviewDialog1.ShowDialog();

            recordDoc.Print();

            foreach (PictureBox picture in fotos)
            {
                if (picture != null)
                {
                    picture.BackColor = System.Drawing.Color.White;
                }
                
            }

            Array.Clear(enlistado,0,enlistado.Length);
            enlistado = new string[150];
            //list.Clear();
            //list = new List<string>(new string[99]);
        }

        //Use a list to save image address and for loop to print on printpage 
        private void PrintPage(object o, PrintPageEventArgs e)
        {
            try {
                using (var memoryStream = new MemoryStream(File.ReadAllBytes(enlistado[page])))
                {
                    using (var image = Image.FromStream(memoryStream))
                    {
                        e.Graphics.DrawImage(image, e.MarginBounds.X / 5, e.MarginBounds.Y - ( (7*e.MarginBounds.Y) /10 ), e.MarginBounds.Width + ( (75 * e.MarginBounds.Width) / 100 ), e.MarginBounds.Height - ( (e.MarginBounds.Height) / 20) ); 

                    }
                }
                      //  System.Drawing.Image img = System.Drawing.Image.FromFile(enlistado[page]);
              
              try
            {
                    string[] filePaths = Directory.GetFiles(@"c:\Users\Chuanyisoft\Pictures\Plantillas\"); // Set address to isacc\Pictures\Plantillas\  
                    using (var memoryStreamer = new MemoryStream(File.ReadAllBytes(filePaths[0])))
                    {
                        using (var plantillas = Image.FromStream(memoryStreamer))
                        {
                            e.Graphics.DrawImage(plantillas, e.MarginBounds.X - ((  e.MarginBounds.X) ), e.MarginBounds.Y + ( (97 * e.MarginBounds.Y) / 30), e.MarginBounds.Width + (100 * e.MarginBounds.Width / 99), (( 41 * e.MarginBounds.Height) / 100 ));
                        }
                    }
            }
            catch (Exception) { MessageBox.Show("No hay plantilla para seleccionar. Favor de contactar al dueño."); }
            page++;
            int count = 0;
            
            for (int c = 0; c < enlistado.Length; c++)
            {
                if (enlistado[c] != "" && enlistado[c] != null)
                {
                    count++;
                }
            }
            e.HasMorePages = page < count;
            }
            catch (Exception) { MessageBox.Show("Ningun imágen fue seleccionada.", "Error"); }
            
        }
        public void generatePictureGrid()
        {
            try
            { //List<string> fileList = new List<string>(new string [900]); 
              //fileList = (Directory.GetFiles(@"C:\Users\Chuanyisoft\Pictures\bluetooth", "*")).OrderByDescending(l => l).ToList(); // isacc\Pictures\bluetooth\



                string[] fotoList = Directory.GetFiles(@"C:\Users\Chuanyisoft\Pictures\bluetooth", "*").OrderByDescending(m => m).ToArray();

                bool primero = true;
                int x = 65;
                int y = 65;
                int contadorPic = 8;
                int i = 0;


                foreach (var item in fotoList)
                {
                    PictureBox pan = new PictureBox();
                    PictureBox pic = new PictureBox();
                    pan.Name = i.ToString();
                    pic.Name = item.ToString();
                    pic.Tag = "dispose";
                    //pic.Tag = "picture";

                    if ((i / contadorPic) == 1)
                    {
                        y += 228;//125   //
                        contadorPic += 8;
                        x = 65;
                    }
                    if (primero)
                    {
                        pan.Location = new Point(x, y);
                        pic.Location = new Point(x, y);
                        primero = false;
                    }
                    else
                    {
                        pan.Location = new Point(x, y);
                        pic.Location = new Point(x, y);
                    }

                    x += 215;// 110;  // 

                    //other properties


                    using (var memoryStreamer = new MemoryStream(File.ReadAllBytes(fotoList[i])))
                    {
                        Image img = System.Drawing.Image.FromStream(memoryStreamer, true, true);
                        memoryStreamer.Seek(0, SeekOrigin.Begin);
                        Bitmap picture = new Bitmap(img);
                        //Bitmap bmp = new Bitmap(picture.Width, picture.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        pic.SizeMode = PictureBoxSizeMode.StretchImage;
                        pic.BorderStyle = BorderStyle.FixedSingle;
                        pic.Width = 209;  //100;    //
                        pic.Height = 222;   //120; //
                        pic.Padding = new Padding(10);
                        pic.BackColor = System.Drawing.Color.White;
                        pic.Image = picture;
                        pic.Tag = item;
                        pic.SizeMode = PictureBoxSizeMode.StretchImage;
                        img.Dispose();
                    }
                    //itemsToRemove.Add(pic);
                    fotos[i] = pic;

                    pic.Click += new System.EventHandler(Pic_Click);

                    void Pic_Click(object o, EventArgs ev)
                    {
                        if (pic.BackColor != System.Drawing.Color.White)
                        {
                            for (int c = 0; c < enlistado.Length; c++)
                            {
                                if (enlistado[c].Contains(pic.Name))
                                {
                                    //list.RemoveAt(c);
                                    enlistado = enlistado.Where(val => val != pic.Name).ToArray();
                                    break;
                                }
                            }
                            pic.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            for (int c = 0; c < enlistado.Length; c++)
                            {
                                if (enlistado[c] == "" || enlistado[c] == null)
                                {
                                    enlistado[c] = pic.Name;
                                    pic.BackColor = System.Drawing.Color.Gold;
                                    break;
                                }
                            }
                        }
                    }
                    panel1.Controls.Add(pic); //agregas el picturebox a la coleccion de controles del form   

                    i++;
                } }
            catch (OutOfMemoryException)
            {
                MessageBox.Show("No se pudo cargar todas las imagenes. Se cargaron solamente las 8 más recientes.");
                throw;
            }
            catch (System.ArgumentException)
            {
                MessageBox.Show("No se pudo cargar todas las imagenes. Se cargaron solamente las 8 más recientes.");
                throw;
            }
        }
        public void deleteGrid()
        {
            foreach (PictureBox picture in fotos)
            {
                if (picture != null)
                {
                    Controls.Remove(picture);
                    picture.Dispose();
                }
                

            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {     
            generatePictureGrid();
        }        
        private void pic_Click(object sender, EventArgs e)
        {
            
        }
        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            
        }
        private void Refrescar_Click(object sender, EventArgs e)
        {
            deleteGrid();
            generatePictureGrid();
            Array.Clear(enlistado, 0, enlistado.Length);
            enlistado = new string[150];
            //list.Clear();
            //list = new List<string>(new string[99]);
        }
    }
}
